﻿using Microsoft.TeamFoundation.SourceControl.WebApi.Legacy;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Mime;
using System.Web;

namespace PushRules
{
    class Program
    {
        static void Main(string[] args)
        {


            var request = new RestRequest(Method.POST)
            .AddHeader("Content-Type", "application/xml")
            //.AddParameter ("Content-Type","application/xml")
            //.AddParameter("backup", @"C:\Users\andrey.vorobev.AD\source\repos\SonarRules\HTML.xml", "application/xml", ParameterType.RequestBody)
            .AddFile("backup", @"C:\Users\andrey.vorobev.AD\source\repos\SonarRules\HTML.xml");


            //request.AddParameter("backup", );
            var client = new RestClient(new Uri("http://dev.sonar-cube.com/sonar/api/qualityprofiles/restore"));
            //client.Timeout = -1;
            client.Authenticator = new HttpBasicAuthenticator("admin", "admin");
            IRestResponse response = client.Execute(request);

            //client.Execute(request);
            Console.WriteLine(response.Content);
            Console.ReadLine();
        }
    }
}

