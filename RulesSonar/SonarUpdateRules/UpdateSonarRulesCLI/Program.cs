﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using LibGit2Sharp;

namespace UpdateSonarRulesCLI
{
    class Program
    {
        static void Main(string[] args)
        {
            var path32 = Environment.SystemDirectory;
            var path = path32 + @"\SonarRules";
            OnStart(path);
        }

        public static void OnStart(string path)
        {
            
            string commitDatePathToFile = path + @"\lastcommitdate.txt";
            if (Directory.Exists(path))
            {
                if (File.Exists(commitDatePathToFile))
                {
                    lastCommitDate(path);

                    string lastsaveddatecommit = File.ReadAllText(commitDatePathToFile);
                    string lastdatecommit = lastCommitDate(path);

                    if (lastsaveddatecommit == lastdatecommit)
                    {
                        File.WriteAllText(commitDatePathToFile, lastdatecommit);
                        //ничо не делаем, можем файлик перезаписать, чтоб что-то делали
                    }
                    else
                    {
                        //Api sonarqube как тебя судова запихнуть((( реез цуи
                    }
                }
                else
                {
                    lastCommitDate(path);
                }
            }
            else
            {
                Directory.CreateDirectory(path);
                Repository.Clone("https://gitlab.com/a.vorobyov/sonarrules", path);
                //записать дату последнего коммита в txt
                lastCommitDate(path);

                // а теперь судова тоже api запихнуть ААА
            }
        }

        public static string lastCommitDate(string path)
        {
            Process p1 = new Process();
            p1.StartInfo.FileName = "cmd.exe";
            p1.StartInfo.WorkingDirectory = path;
            p1.StartInfo.Arguments = "/c git log -1 --format=%cd origin/master > lastcommitdate.txt";
            p1.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p1.StartInfo.Verb = "runas";
            p1.StartInfo.UseShellExecute = false;
            p1.StartInfo.RedirectStandardOutput = true;
            p1.Start();
            StreamReader reader = p1.StandardOutput;
            string lastdatecommit = reader.ReadToEnd();
            p1.WaitForExit();
            p1.Close();            
            return lastdatecommit;
        }
    }    
}
