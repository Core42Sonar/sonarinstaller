﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SonarUpdateRules
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            string path = Environment.SystemDirectory + @"\SonarRules";
            
            if (Directory.Exists(path))
            {
                Repository.Clone("https://gitlab.com/a.vorobyov/sonarrules", path);
                string commitDatePathToFile = path + @"\lastcommitdate.txt";
                
                if (File.Exists(commitDatePathToFile))
                {
                    Process p1 = new Process();
                    p1.StartInfo.FileName = "cmd.exe";
                    p1.StartInfo.WorkingDirectory = path;
                    p1.StartInfo.Arguments = "/c git log - 1--format =% cd origin / master";
                    p1.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p1.Start();

                    string lastsaveddatecommit = File.ReadAllText(commitDatePathToFile);
                    string lastdatecommit = p1.StandardOutput.ReadToEnd();

                    if (lastsaveddatecommit == lastdatecommit)
                    {
                        File.WriteAllText(commitDatePathToFile, lastdatecommit);
                        //ничо не делаем, можем файлик перезаписать, чтоб что-то делали
                    }
                    else
                    {
                        //Api sonarqube как тебя судова запихнуть((( реез цуи
                    }
                    
                }
                else
                {
                    //записать дату последнего коммита в txt
                    Process p = new Process();
                    p.StartInfo.FileName = "cmd.exe";
                    p.StartInfo.WorkingDirectory = path;
                    p.StartInfo.Arguments = "/c git log -1 --format=%сd origin/master > " + commitDatePathToFile;
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p.Start();

                    // а теперь судова тоже api запихнуть ААА
                }
            }
            else
            {
                Directory.CreateDirectory(path);

            }

            using (var repo = new Repository(path))
            {
                Commit commit = repo.Lookup<Commit>("");
                Console.WriteLine("Author: {0}", commit.Author.Name);
                Console.WriteLine("Message: {0}", commit.MessageShort);
            }
            Repository.Clone("https://gitlab.com/a.vorobyov/sonarrules", path);
            

        }

        protected override void OnStop()
        {
        }

        public void UpdateRules()
        {
            //качать репозиторий пушить правила в сонар
        }
    }
}
