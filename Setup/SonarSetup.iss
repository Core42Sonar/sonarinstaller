[Files]
Source: "dockercluster\*"; DestDir: "{app}";Flags: ignoreversion recursesubdirs
Source: "dockercluster\SonarHosts.ps1"; DestDir: "{app}";Flags: 64bit; Permissions: admins-full
Source: "dockercluster\SonarHost.bat"; DestDir: "{userstartup}"; Permissions: admins-full


[Run]
;Filename: "{app}\CreateCluster.ps1";Parameters: "{app} {sys}" ; Tasks: StartAfterInstall
Filename: "PowerShell";Parameters: "-NoProfile -ExecutionPolicy Unrestricted -Command ""& {{Start-Process PowerShell -ArgumentList '-NoProfile -ExecutionPolicy Unrestricted -File {app}\CreateCluster.ps1 {app} {sys}'-Verb RunAs}"""; Tasks: StartAfterInstall;Flags: 64bit

[Icons]
Name: "{commondesktop}\RunSonarFramework"; Filename: "{app}\SonarScanerNetFramework\RunSonarFramework.bat"; Parameters: ""; \
IconFilename: C:\Users\andrey.vorobev.AD\Desktop\Newfolder\Setup\SonarLogo.ico;
Name: "{commondesktop}\RunSonarCore"; Filename: "{app}\SonarScanerNetCore\RunSonarCore.bat"; Parameters: ""; \
IconFilename: C:\Users\andrey.vorobev.AD\Desktop\Newfolder\Setup\SonarLogo.ico;
Name: "{commonstartup}\UpdateHosts"; Filename: "{app}\SonarHost.bat";  Parameters: "{app}"

[Tasks]
Name: StartAfterInstall; Description: ��������� ���������� ����� ���������

[Setup]
PrivilegesRequired=admin
AppName=SonarQube
AppVersion=1.0
WizardStyle=modern
DefaultDirName={sd}\42clouds\SonarSetup
AppId=F769CF58-3E9E-4BB8-A3E6-90F277554F28
ShowLanguageDialog=no
DefaultGroupName=42clouds
SetupIconFile=C:\Users\andrey.vorobev.AD\Desktop\Newfolder\Setup\SonarLogo.ico
AlwaysShowComponentsList=False
ShowComponentSizes=False
OutputBaseFilename=Sonar42Setup
UninstallDisplayIcon={app}\Sonar42.exe
SolidCompression=False
CloseApplications=force
UsePreviousTasks=yes
PrivilegesRequiredOverridesAllowed=dialog
;MinVersion=1.0
;SignTool="C:\Program Files (x86)\Microsoft SDKs\ClickOnce\SignTool\signtool.exe" sign /f "C:\certLink42\UCHETONLINE_LLC.pfx" /t http://timestamp.comodoca.com/authenticode /p 46iyHQ88sGeh $f
SignTool=SignTool

[UninstallDelete]
Type: filesandordirs; Name: "{userdocs}\Sonar42"
Type: files; Name: "\Sonar42\Sonar42.xml"
Type: filesandordirs; Name: "{app}"

[Languages]
Name: "Ru"; MessagesFile: "compiler:Languages\Russian.isl"
