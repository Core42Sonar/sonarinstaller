Param (
[Parameter (Mandatory=$true, Position=1)]
[string]$projPath
)
$a=$projPath
if ($a.EndsWith("\") )
{
    $key=$a.TrimEnd("\")
    $key=$key.Split('\')[-1]
}
else
{
$key=$a.Split('\')[-1]
}
.\SonarScanner.MSBuild.exe begin /k:$key 
MSBuild.exe $projPath
.\SonarScanner.MSBuild.exe end 