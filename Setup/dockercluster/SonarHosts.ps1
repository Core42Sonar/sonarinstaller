﻿minikube start
$name = "dev.sonar-cube.com"
$minikubeip =& minikube ip 
$file = "C:\Windows\System32\drivers\etc\hosts"

function Replace-TextInFile
{
    Param(
        [string]$FilePath,
        [string]$Pattern,
        [string]$Replacement
    )

    [System.IO.File]::WriteAllText(
        $FilePath,
        ([System.IO.File]::ReadAllText($FilePath) -replace $Pattern, $Replacement)
    )
}

function setConfig( $file, $key, $value ) {
    $content = Get-Content $file
    if ( $content -match "\S+\s+$key" ) {    
        Replace-TextInFile $file "\S+\s+$key" "$value $key"   
    } else {
        Add-Content $file "`n$value $key"
    }
}

setConfig "C:\Windows\System32\drivers\etc\hosts" "dev.sonar-cube.com" $minikubeip


