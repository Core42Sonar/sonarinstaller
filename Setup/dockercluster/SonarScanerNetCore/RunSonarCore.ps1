Param (
[Parameter (Mandatory=$true, Position=1)]
[string]$projPath
)
$a=$projPath
if ($a.EndsWith("\") )
{
    $key=$a.TrimEnd("\")
    $key=$key.Split('\')[-1]
}
else
{
$key=$a.Split('\')[-1]
}
dotnet .\SonarScanner.MSBuild.dll begin /k:$key
dotnet build $projPath
dotnet .\SonarScanner.MSBuild.dll end