﻿Param (
[Parameter (Mandatory=$true, Position=1)]
[string]$pathScripts,
[Parameter (Mandatory=$true, Position=2)]
[string]$pathSys32
)
minikube start --memory 4096 
minikube kubectl
minikube ssh "echo 'sysctl vm.max_map_count=262144' | sudo tee -a /var/lib/boot2docker/bootlocal.sh"
minikube ssh "sudo sysctl vm.max_map_count=262144"
minikube ssh "echo 'sysctl -p'"
#sudo sysctl -w vm.max_map_count=262144
#exit
cd $pathSys32
cmd /c curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.16.0/bin/windows/amd64/kubectl.exe
kubectl get nodes
cd $pathScripts
kubectl create secret generic postgres-pwd --from-literal=password=CodeRise_Pass
kubectl create -f sonar-pv-postgres.yaml
kubectl create -f sonar-pvc-postgres.yaml
kubectl create -f sonar-postgres-deployment.yaml
kubectl create -f sonarqube-deployment.yaml
kubectl create -f sonarqube-service.yaml
kubectl create -f sonar-postgres-service.yaml
minikube addons enable ingress
kubectl apply -f ingress-controller.yaml
kubectl get po -o wide
.\SonarHosts.ps1

